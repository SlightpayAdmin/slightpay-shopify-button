# Botón de SlightPay para Shopify
## ¿Cómo usar el botón para Shopify?
Para utilizar el código del botón es necesario que tu Ecommerce haya sido creado en Shopify y tengas acceso al apartado Additional Scripts o Scripts Adicionales tal y como se muestra en la [documentación](https://integracion.slightpay.com/#/shopify).

## Antes de empezar
Si vas a reemplazar algún código previo que hayas tenido almacenado en la seccion Additional Scripts de Shopify lo mejor seria copiar y guardar ese código en un archivo (Hacer respaldo del contenido) y después de eso pegar el botón de SlightPay, así si no funciona puedes regresar a tu código anterior.

Asegurate de cambiar tu ID y tu llave que previamente debes conocer desde la sección comercios, es la línea número 1 la que deberás modificar.
Ejemplo: En la linea 12 y 13 de la imagen se puede observar que la llave y el ID fueron reemplazados.
No olvides reemplazar  del código tu id de comercio y llave de producción respectivamente (Se obtienen del panel de comercio).
![Imagen 1](https://gitlab.com/SlightpayAdmin/slightpay-shopify-button/-/raw/master/img2button.png)

## Tests
Una vez que hayas leído la documentación del botón hay que asegurarnos de unas pruebas (realizar una compra en tu sitio y verificar los costos una vez que abra la pantalla de pagos.slightpay) las cuales nos ayudan a identificar si el código default para Shopify funciona o hay que realizar ciertas modificaciones, normalmente no debería haber correcciones pero en ocasiones temas como el diseño de la página afectan directamente las variables que nos manda Shopify a SlightPay, en ese caso si no cuentas con un desarrollador para modificar el código puedes escribirnos directamente al correo y atenderemos tu duda, escribenos al correo [soporte@slightpay.com](mailto:soporte@slightpay.com).
